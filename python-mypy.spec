%global _empty_manifest_terminate_build 0
Name:		python-mypy
Version:	1.13.0
Release:	1
Summary:	Optional static typing for Python
License:	MIT and Python-2.0
URL:		https://github.com/python/mypy
Source0:	https://github.com/python/mypy/archive/refs/tags/v%{version}.tar.gz

Requires:	python3-typed-ast
Requires:	python3-typing-extensions
Requires:	python3-mypy-extensions
Requires:	python3-tomli
Requires:	python3-psutil

%description
Add type annotations to your Python programs, and use mypy to type
check them.  Mypy is essentially a Python linter on steroids, and it
can catch many programming errors by analyzing your program, without
actually having to run it.  Mypy has a powerful type system with
features such as type inference, gradual typing, generics and union
types.

%package -n python3-mypy
Summary:	Optional static typing for Python
Provides:	python-mypy
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
BuildRequires:	python3-cffi
BuildRequires:	gcc
BuildRequires:  python3-pip
BuildRequires:  python3-pbr
BuildRequires:  help2man
%description -n python3-mypy
Add type annotations to your Python programs, and use mypy to type
check them.  Mypy is essentially a Python linter on steroids, and it
can catch many programming errors by analyzing your program, without
actually having to run it.  Mypy has a powerful type system with
features such as type inference, gradual typing, generics and union
types.

%package help
Summary:	Development documents and examples for mypy
Provides:	python3-mypy-doc
%description help
Add type annotations to your Python programs, and use mypy to type
check them.  Mypy is essentially a Python linter on steroids, and it
can catch many programming errors by analyzing your program, without
actually having to run it.  Mypy has a powerful type system with
features such as type inference, gradual typing, generics and union
types.

%prep
%autosetup -n mypy-%{version}

%build
%py3_build

%install
%py3_install
rm -vrf %{buildroot}%{python3_sitelib}/mypy/{test,typeshed/tests}
ln -s /usr/share/typeshed %{buildroot}%{python3_sitelib}/mypy/typeshed
 
# Generate man pages
mkdir -p %{buildroot}%{_mandir}/man1
PYTHONPATH=%{buildroot}%{python3_sitelib} \
    help2man --no-info --version-string 'mypy %{version}-dev' \
        --no-discard-stderr -o %{buildroot}%{_mandir}/man1/mypy.1 \
        %{buildroot}%{_bindir}/mypy
 
PYTHONPATH=%{buildroot}%{python3_sitelib} \
    help2man --no-info --version-string 'mypy stubgen %{version}-dev' \
        --no-discard-stderr -o %{buildroot}%{_mandir}/man1/stubgen.1 \
        %{buildroot}%{_bindir}/stubgen

install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-mypy -f filelist.lst
%{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Wed Oct 23 2024 lixiaoyong <lixiaoyong@kylinos.cn> - 1.13.0-1
- Upgrade to version 1.13.0
  - Significantly speed up file handling error paths (Shantanu, PR [17920]
  - Use fast path in modulefinder more often (Shantanu, PR [17950]
  - Let mypyc optimise os.path.join (Shantanu, PR [17949]
  - Make is_sub_path faster (Shantanu, PR [17962]

* Mon Aug 19 2024 wangkai <13474090681@163.com> - 1.11.1-1
- Update to 1.11.1

* Tue May 7 2024 tenglei <tenglei@kylinos.cn> - 1.10.0-1
- upgrade package to 1.10.0
- Enum private attributes are not enum members

* Thu Jan 11 2024 Paul Thomas <paulthomas100199@gmail.com> - 1.8.0-1
- update to version 1.8.0

* Thu Jul 06 2023 niuyaru <niuyaru@kylinos.cn> - 1.4.1-1
- Upgrade package to version 1.4.1

* Fri Jun 02 2023 chendexi <wu_lei@hoperun.com> - 1.2.0-1
- Upgrade package to version 1.2.0

* Tue Dec 6 2022 lijian <lijian2@kylinos.cn> - 0.991-1
- Upgrade package to version 0.991

* Tue Oct 11 2022 guozhengxin <guozhengxin@kylinos.cn> - 0.982-1
- Upgrade package to version 0.982

* Fri May 06 2022 xigaoxinyan <xigaoxinyan@h-partners.com> - 0.901-1
- Upgrade to 0.901

* Sat Aug 14 2021 huangtianhua <huangtianhua@huawei.com> -0.812-1
- Upgrade to 0.812 to support OpenStack-W

* Wed Aug 04 2021 chenyanpanHW <chenyanpan@huawei.com> - 0.800-2
- DESC: delete BuildRequires gdb

* Mon Feb 01 2021 yanglongkang <yanglongkang@huawei.com> - 0.800-1
- Package init
